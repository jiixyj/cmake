orangec-compiler
----------------

* The OrangeC compiler is now supported with
  :variable:`compiler id <CMAKE_<LANG>_COMPILER_ID>` ``OrangeC``.
